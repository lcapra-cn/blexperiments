
var noble = require('noble');
var async = require('async');

var peripheralUuid = process.argv[2];

noble.on('stateChange', function(state) {
    if (state === 'poweredOn') {
        noble.startScanning();
    } else {
        noble.stopScanning();
    }
});

if(!process.argv[2]) {
    console.log("BLE device uuid not specified! ");
}

process.on('SIGINT', function() {
    console.log("Stop scanning");
    noble.stopScanning();
});

noble.on('discover', function(peripheral) {

//    if(!process.argv[2]) {
        console.log("Found %s", peripheral.uuid);
//    }

    if (!process.argv[2] || peripheral.uuid === peripheralUuid) {

        noble.stopScanning();

        console.log('peripheral with UUID ' + peripheral.uuid + ' found');
        var advertisement = peripheral.advertisement;

        var localName = advertisement.localName;
        var txPowerLevel = advertisement.txPowerLevel;
        var manufacturerData = advertisement.manufacturerData;
        var serviceData = advertisement.serviceData;
        var serviceUuids = advertisement.serviceUuids;

        if (localName) {
            console.log('  Local Name        = ' + localName);
        }

        if (txPowerLevel) {
            console.log('  TX Power Level    = ' + txPowerLevel);
        }

        if (manufacturerData) {
            console.log('  Manufacturer Data = ' + manufacturerData.toString('hex'));
        }

        if (serviceData) {
            console.log('  Service Data      = ', serviceData);
        }

        if (localName) {
            console.log('  Service UUIDs     = ' + serviceUuids);
        }


//        peripheral.updateRssi(function(err, rssi) {
//            console.log("rssi update!");
//            console.log(rssi);
//        });

        console.log();
        peripheral.connect(function(error) {

            console.log("» Connected");

            process.on('SIGINT', function() {

                console.log("Disconnect");
                peripheral.disconnect(function(){
                    console.log("Bye");
                    process.exit();
                });

            });

            if(error) {
                console.warn(error);
                process.emit('SIGINT');
            }

//            explore(peripheral);

            peripheral.discoverServices([], function(err, services) {

                err && console.warn(err);

//
//                peripheral.readHandle(0x0012, function(err, res) {
//                    console.error("Read handle 0x0012 ", err, res)
//                });

                services.forEach(function(service) {

                    service.discoverIncludedServices([], function(err, includedServices) {
                        includedServices.forEach(function(includedService) {
                            console.log("   » includedService %s", includedService.uuid);
                        });
                    });

                    service.discoverCharacteristics([], function(err, characteristics) {

                        err && console.warn(err);

                        console.log("» Service %s", service.uuid);
                        characteristics.forEach(function(characteristic) {

                            console.log("   » char %s", characteristic.uuid);

                            characteristic.discoverDescriptors(function(error, descriptors) {
                                descriptors.forEach(function(descriptor) {
                                    console.log("       » descriptor %s", descriptor.uuid);
                                });
                            });

//                            console.log("       » char %s: read", characteristic.uuid);
//                            characteristic.read(function(error, data) {
//                                console.log("       » char %s: read %s (raw: %s)", characteristic.uuid, data.toString('UTF8'), data);
//                            });

                            var notify = true;
                            console.log("       » char %s: setup notify to ", characteristic.uuid, notify);
                            characteristic.notify(notify, function(error) {
                                error && console.log("       » char %s: notify err", characteristic.uuid, error);
                                console.log("       » char %s: notify ok", characteristic.uuid);
                            });

                            characteristic.on('notify', function(data) {
                                console.log("       » char " + characteristic.uuid + " notification:", data.toString(), ' RAW:', data);
                            });

                            characteristic.on('read', function(data, isNotification) {
                                console.log("       » char " + characteristic.uuid + " read:", data.toString(), ' RAW:', data, 'isNotification', isNotification);
                            });

                        });
                    })

                });


            }); // any service UUID


        });

    }
});
