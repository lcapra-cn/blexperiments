/**
 * Copyright 2014 Luca Capra <luca.capra@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

var bletools = require(__dirname + '/../bletools');

module.exports = function(RED) {

    function BLEwrite(n) {

        // Create a RED node
        RED.nodes.createNode(this, n);

        var node = this;
        var msg = {
            topic: this.topic,
            payload: null,
            raw: null,
            error: null
        };

        //get name and uuid from user
        this.ble_notify_device = RED.nodes.getNode(n.ble_notify_device);
        this.ble_notify_char = n.ble_notify_char;

        var deviceUuid = this.ble_notify_device.ble_device_uuid;
        var selectedChar = this.ble_notify_char;

        deviceUuid = deviceUuid.replace(':', '', 'g').replace(' ', '', 'g').toLowerCase();
        selectedChar = selectedChar.replace('0x', '').replace(' ', '', 'g').toLowerCase();

        node.log("deviceUuid "+deviceUuid+", selectedChar " + selectedChar);

        var device = new bletools.Device(node);

        device.on('error', function(err) {
            node.error(JSON.stringify(err));
        });

        var selectedCharateristic;
        var onInput = function(inputMsg) {

//            node.log("INPUT! " + JSON.stringify(inputMsg));

            var _write = function() {

                var notify = false;
                selectedCharateristic.write(new Buffer(inputMsg.payload), notify, function(err) {

                    if(err) {
                        node.error("Error writing to " + char.uuid);
                        return;
                    }

                    node.log("Write ok");

                });

            };

            if(!selectedCharateristic) {

                var peripheralInfo = device.getPeripheral(deviceUuid);
                if(peripheralInfo) {

                    node.log("Found device " + peripheralInfo.uuid + " / Looking for " + deviceUuid);

                    if(peripheralInfo.uuid !== deviceUuid) {
                        return;
                    }

                    node.log("Found requested device " + peripheralInfo.uuid);

                    var found = false;
                    peripheralInfo.characteristics.forEach(function(char) {

                        if(found) {
                            return;
                        }

                        node.log("found char.uuid "+ char.uuid.toString() +" looking for " + selectedChar);
                        if(char.uuid.toString() === selectedChar) {

                            node.log("found uuid " + char.uuid.toString());
                            found = true;

                            selectedCharateristic = char;
                            _write();
                        }
                    });

                };

            }
            else {
                _write();
            }

        };

        this.on("input", function() {

            try {
                onInput.apply(node, arguments);
            }
            catch(e) {
                node.error("An error occured");
                node.error(JSON.stringify(e));
            }
        });

        this.on("close", function() {
            node.log("Closing BLEwrite");
            device.disconnect();
        });

    }
    // Register the node by name. This must be called before overriding any of the
    // Node functions.
    RED.nodes.registerType("BLEwrite", BLEwrite);
};