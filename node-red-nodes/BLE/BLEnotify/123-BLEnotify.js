/**
 * Copyright 2014 Luca Capra <luca.capra@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

var bletools = require(__dirname + '/../bletools');

module.exports = function(RED) {

    //import noble
    var noble = require('noble');

    // The main node definition - most things happen in here
    function BleNotify(n) {

        // Create a RED node
        RED.nodes.createNode(this, n);

        var node = this;
        var msg = {
            topic: this.topic,
            payload: null,
            raw: null,
            error: null
        };

        //get name and uuid from user
        this.ble_notify_device = RED.nodes.getNode(n.ble_notify_device);
        this.ble_notify_char = n.ble_notify_char;

        var deviceUuid = this.ble_notify_device.ble_device_uuid;
        var selectedChar = this.ble_notify_char;

        deviceUuid = deviceUuid.replace(':', '', 'g').replace(' ', '', 'g').toLowerCase();
        selectedChar = selectedChar.replace('0x', '').replace(' ', '', 'g').toLowerCase();

        node.log("deviceUuid "+deviceUuid+", selectedChar " + selectedChar);

        var onClose = null;
        var device = new bletools.Device(this);
        device.on("peripheral", function(peripheralInfo) {

            if(peripheralInfo.uuid !== deviceUuid) {
                return;
            }

            node.log("Found requested device " + peripheralInfo.uuid);

            var found = false;
            peripheralInfo.characteristics.forEach(function(char) {

                if(found) {
                    return;
                }

                node.log("found char.uuid "+ char.uuid.toString() +" looking for " + selectedChar);
                if(char.uuid.toString() === selectedChar) {

                    node.log("found uuid " + char.uuid.toString());
                    found = true;

                    char.notify(true, function(err) {

                        if(err) {
                            node.error("Error registering notify for " + char.uuid +": "+ JSON.stringify(err));
                        }

                    });

                    char.on('read', function(data, isNotification) {
                        if(isNotification) {

                            msg.raw = data;
                            msg.payload = new Buffer(data).toString();

                            node.log("Got data: " + msg.payload + "RAW: " + msg.raw );
                            node.send(msg);
                        }
                    });
                }
            });

        });

//        this.on("input", function(msg) {
//        });

        this.on("close", function() {
            node.log("Closing BLEnotify");
            device.disconnect();
        });

    }
    // Register the node by name. This must be called before overriding any of the
    // Node functions.
    RED.nodes.registerType("BLEnotify", BleNotify);
};