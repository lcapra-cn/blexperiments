module.exports = function(grunt) {


    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-multisync');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('sync', function (spec) {
       grunt.task.run(['multisync:local']);
    });

    grunt.initConfig({

        watch: {
          scripts: {
            files: ['**/*.js'],
            tasks: ['sync'],
            options: {
              spawn: false,
            },
          },
        },

      multisync: {
        options: {
            drives: {
                prjsrc: "~",
                pi: "/home/l/mnt/pi/",
            }
        },
        // Configure a job to back up multiple folder pairs from one drive to another
        local: {
            drives: {
//                src:    '<%= multisync.options.drives.prjsrc %>',
                src:    '~',
                dest:   '<%= multisync.options.drives.pi %>'
            },
            folders: [
                {
                    src: '/',
                    dest: 'blexperiments/'
                },
            ],
            options: {
                // any rsyncwrapper options here get allpied to ALL folder pairs
                recursive: true,
                dryRun: false,
                exclude: [ "node_modules/*" ]
            }
        }
      },
    })

    // Default task(s).
    grunt.registerTask('default', ['sync']);

};
